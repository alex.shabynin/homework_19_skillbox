// Homework_19_Skillbox.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;

// ������ ������� � ������� ������������� �������� ������ �������� �� ������� �������. ���������������.
class Animal
{
public:
	virtual void Voice()
    {
        
    }
};

class Dog : public Animal
{
public:
	void Voice() override
	{
        cout << "The dog says: Gav-Gav" << endl;
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		cout << "The cat says: Meow-Meow" << endl;
	}
};

class Crow : public Animal
{
public:
	void Voice() override
	{
		cout << "The crow says: Karr-Karr" << endl;
	}
};

int main()
{
	Dog DogTalk;
	Cat CatTalk;
	Crow CrowTalk;
	int v = 0;

	Animal* arr[3] = { &DogTalk, &CatTalk, &CrowTalk };

	for (int i = 0; i < 3; i++)
	{
		arr[i]->Voice();
	}

	cout << "Enter number of the animal (0-2): ";
	cin >> v ;
	cout << endl;

	for (int i = 0; i < 3; i++)
	{
		if (i == v)
		{
			arr[i]->Voice();
		}
	}

    return 0;
}


//������ �������, � ������� ������������ ������ �������� ��������� � ��� �����. ���� ���������������
/*class Animal
{
public:
	virtual void Voice()
	{

	}
};

class FirstAnimal : public Animal
{
public:
	void Voice() override
	{
		char a1[40] = "name";
		char v1[40] = "voice";
		
		cout << "Please specify your first animal: ";
		cin >> a1;
		cout << "\nAnd what voice that animal has: ";
		cin >> v1;
		cout << "The "<<a1 << " says: " <<v1 << endl;
	}
};

class SecondAnimal : public Animal
{
public:
	void Voice() override
	{
		char a1[40] = "name";
		char v1[40] = "voice";

		cout << "Please specify your second animal: ";
		cin >> a1;
		cout << "\nAnd what voice that animal has: ";
		cin >> v1;
		cout << "The " << a1 << " says: " << v1 << endl;
	}
};

class ThirdAnimal : public Animal
{
public:
	void Voice() override
	{
		char a1[40] = "name";
		char v1[40] = "voice";

		cout << "Please specify your third animal: ";
		cin >> a1;
		cout << "\nAnd what voice that animal has: ";
		cin >> v1;
		cout << "The " << a1 << " says: " << v1 << endl;
	}
};



int main()
{
	FirstAnimal FirstTalk;
	SecondAnimal SecondTalk;
	ThirdAnimal ThirdTalk;
	int v = 0;

	Animal* arr[3] = { &FirstTalk, &SecondTalk, &ThirdTalk };

	for (int i = 0; i < 3; i++)
	{
		arr[i]->Voice();
	}

	
	return 0;
}*/

